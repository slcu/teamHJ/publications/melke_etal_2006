## Introduction

Files containing model description and parameter values resulting from the optimisation
analysed in the paper

Melke, P., Pardali, E., ten Dijke, P., Peterson, C. (2006)
[A Rate Equation Approach to Elucidate the Kinetics and Robustness of the TGF-β Pathway.](https://www.sciencedirect.com/science/article/pii/S0006349506721506)
Biophysical Journal 91, 4368-4380.

## Files

The <tt>modelFiles</tt> directory holds two files:

<p><tt>ensemble.data</tt> - this file contains all 32 parameter sets extracted
and used for the analysis.</p>

<p><tt>tgfb.model</tt> - a model description file that can be used for simulating 
the model using the [organism](https://gitlab.com/slcu/teamhj/organism) software.

## Contact

<p>henrik.jonsson@slcu.cam.ac.uk</p>
<p>carsten@thep.lu.se</p>
